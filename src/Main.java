import controller.DBManager;
import entities.User;
import menu.MainMenu;
import menu.WelcomeMenu;

public class Main {

    public static void main(String[] args) {

        WelcomeMenu welcomeMenu = new WelcomeMenu();
        User user = welcomeMenu.goMenu();

        MainMenu mainMenu = new MainMenu();
        mainMenu.goMenu(user);
    }
}
