package entities;

public class User {

    private int id;
    private String name;
    private String surname;
    private int age;
    private String email;
    private String login;
    private String password;
    private int money;

    public User() {
    }

    public User(int id, String name, String surname, int age, String email, String userName, String password, int money) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.email = email;
        this.login = userName;
        this.password = password;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String toString(){
        return id + ")" + login + ". " + "Name: " + name + ", surname: " + surname +
                ", age: " + age + ", email: " + email + ". Balance: $" + money + ".";
    }
}
