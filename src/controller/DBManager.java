package controller;

import entities.Good;
import entities.User;

import java.sql.*;
import java.util.ArrayList;

public class DBManager {

    private Connection connection;

    public void connect() {

        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/Test_DB";
            String login = "postgres";
            String password = "UkBu7tpfkThTghJ$";
            connection = DriverManager.getConnection(url, login, password);
            System.out.println("[Connected to the PostgreSQL server successfully]");

            CreateUsersTable();
            CreateGoodsTable();
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    private void CreateUsersTable() {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS users " +
                            "(id SERIAL, " +
                            " name VARCHAR(50), " +
                            " surname VARCHAR (50), " +
                            " age INTEGER not NULL, " +
                            " email VARCHAR (50), " +
                            " login VARCHAR (50), " +
                            " password VARCHAR (50), " +
                            " money INTEGER not NULL, " +
                            " PRIMARY KEY (id))"
            );

            int rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CreateGoodsTable() {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS goods " +
                            "(id SERIAL, " +
                            " name VARCHAR(50), " +
                            " price INTEGER, " +
                            " quantity INTEGER, " +
                            " PRIMARY KEY (id))"
            );

            int rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                int age = resultSet.getInt("age");
                String email = resultSet.getString("email");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                int money = resultSet.getInt("money");
                users.add(new User(id, name, surname, age, email, login, password, money));
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public ArrayList<Good> getGoodsAvailable() {
        ArrayList<Good> goods = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM goods WHERE quantity > 0 ORDER BY id");

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int price = resultSet.getInt("price");
                int quantity = resultSet.getInt("quantity");
                goods.add(new Good(id, name, price, quantity));
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return goods;
    }

    public void AddUser(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO users (name, surname, age, email, login, password, money) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)"
            );

            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setInt(3, user.getAge());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getLogin());
            statement.setString(6, user.getPassword());
            statement.setInt(7, user.getMoney());

            int rows = statement.executeUpdate();
            statement.close();
            System.out.println("[User added succesfully]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User SearchUser(String login, String password) {
        User user = new User();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM users WHERE login = " + addQuotes(login) + " AND password = " + addQuotes(password)
            );
            while (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setAge(resultSet.getInt("age"));
                user.setEmail(resultSet.getString("email"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setMoney(resultSet.getInt("money"));
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user.getName() != null) {
            return user;
        } else {
            return null;
        }
    }

    private String addQuotes(String word) {
        return "'" + word + "'";
    }

    public void userMakesBuy(User user, Good good) {
        if (user.getMoney() >= good.getPrice()) {
            decreaseUsersMoney(user, good);
            decreaseGoodsQuantity(good);
        } else {
            System.out.println("You haven't enough money");
        }
    }

    private void decreaseUsersMoney(User user, Good good) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE users SET money = ? WHERE id = ?"
            );
            statement.setInt(1, user.getMoney() - good.getPrice());
            statement.setInt(2, user.getId());

            int rows = statement.executeUpdate();
            statement.close();
            System.out.println("[You have bought " + good.getName() + "]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void decreaseGoodsQuantity(Good good) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE goods SET quantity = ? WHERE id = ?"
            );
            statement.setInt(1, good.getQuantity() - 1);
            statement.setInt(2, good.getId());

            int rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
