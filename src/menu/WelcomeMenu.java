package menu;

import controller.DBManager;
import entities.User;
import sun.applet.Main;

import java.util.Optional;
import java.util.Scanner;

public class WelcomeMenu {
    private DBManager manager = new DBManager();
    private Scanner in = new Scanner(System.in);

    public User goMenu() {
        manager.connect();

        System.out.println("Greetings!");

        while (true) {
            System.out.println("\n Chose an action:" +
                    "\n [1] Authorization" +
                    "\n [2] Registration" +
                    "\n [0] Exit");
            String choice = in.next();
            switch (choice) {
                case ("1"):
                    User authorizedUser = authorization();
                    if (authorizedUser!=null) return authorizedUser;
                    break;
                case ("2"):
                    registration();
                    break;
                case ("0"):
                    System.out.println("Bye-bye");
                    System.exit(1);
                    break;
                default:
                    System.out.println("Unknown command, please try again");
                    break;
            }
        }
    }

    private User authorization() {
        System.out.println("Enter login");
        String login = in.next();
        System.out.println("Enter password");
        String password = in.next();

        User authorizedUser = manager.SearchUser(login, password);

        if (authorizedUser != null) {
            return authorizedUser;
        } else {
            System.out.println("Incorrect data");
        }
        return null;
    }

    private void registration() {
        User user = new User();

        System.out.println("Enter your name");
        user.setName(in.next());

        System.out.println("Enter your surname");
        user.setSurname(in.next());

        System.out.println("Enter your age");
        int age = getNumber(in.next());
        System.out.println(age);

        if (age >= 0) {
            user.setAge(age);
        } else {
            user.setAge(age);
            System.out.println("Set min age (0)");
        }

        System.out.println("Enter your email");
        user.setEmail(in.next());

        System.out.println("Enter your login");
        user.setLogin(in.next());

        System.out.println("Enter your password");
        user.setPassword(in.next());

        System.out.println("Enter your amount of money");
        int money = getNumber(in.next());
        if (money >= 0) {
            user.setMoney(money);
        } else {
            user.setMoney(money);
            System.out.println("Set no money (0)");
        }

        manager.AddUser(user);
    }

    public static int getNumber(String enter) {
        Scanner in = new Scanner(System.in);
        try {
            int number = Integer.parseInt(enter.trim());
            return number;
        } catch (Exception e) {
            System.out.println("Enter a number please");
            return getNumber(in.next());
        }
    }
}
