package menu;

import controller.DBManager;
import entities.Good;
import entities.User;

import java.util.ArrayList;
import java.util.Scanner;

public class MainMenu {
    private DBManager manager = new DBManager();
    private Scanner in = new Scanner(System.in);
    private User user;

    public void goMenu(User user) {
        manager.connect();
        this.user = user;

        while (true) {
            System.out.println("\nWhat would you like to do?" +
                    "\n [1] Show all users" +
                    "\n [2] Buy a good" +
                    "\n [0] Exit");
            String choice = in.next();
            switch (choice) {
                case ("1"):
                    showAllUsers();
                    break;
                case ("2"):
                    buyGood();
                    break;
                case ("0"):
                    System.out.println("Bye-bye");
                    System.exit(1);
                    break;
                default:
                    System.out.println("Unknown command, please try again");
                    break;
            }
        }
    }

    private void showAllUsers() {
        System.out.println("There are all users for now:");

        ArrayList<User> users = manager.getUsers();
        if (!users.isEmpty()) {
            for (User user : users) {
                System.out.println(user.toString());
            }
        } else {
            System.out.println("Oops, we have no users yet");
        }
    }

    private void buyGood() {
        System.out.println("Your balance is $" + user.getMoney() +
                "\nWe have:");
        ArrayList<Good> goods = manager.getGoodsAvailable();
        if (!goods.isEmpty()) {
            for (Good value : goods) {
                System.out.println(value.toString());
            }

            System.out.println("Enter id of a good you want to buy");
            int goodId = -1;
            while (true) {
                goodId = WelcomeMenu.getNumber(in.next());
                if (goodId >= goods.size() + 1 || goodId <= 0) {
                    System.out.println("Incorrect ID, try again");
                } else {
                    break;
                }
            }

            Good good = getGoodById(goodId, goods);

            try {
                manager.userMakesBuy(user, good);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            user = manager.SearchUser(user.getLogin(), user.getPassword());
        } else {
            System.out.println("...no goods yet");
        }
    }

    private Good getGoodById(int id, ArrayList<Good> goods) {
        int low = 0;
        int high = goods.size();
        while (low <= high) {
            int mid = (low + high) / 2;
            if (goods.get(mid).getId() < id) {
                low = mid + 1;
            } else if (goods.get(mid).getId() > id) {
                high = mid - 1;
            } else if (goods.get(mid).getId() == id) {
                return goods.get(mid);
            }
        }
        return null;
    }
}
